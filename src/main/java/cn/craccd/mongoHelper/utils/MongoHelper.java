package cn.craccd.mongoHelper.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.core.convert.QueryMapper;
import org.springframework.data.mongodb.core.convert.UpdateMapper;
import org.springframework.data.mongodb.core.mapping.MongoPersistentEntity;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import cn.craccd.mongoHelper.bean.CreateTime;
import cn.craccd.mongoHelper.bean.InitValue;
import cn.craccd.mongoHelper.bean.Page;
import cn.craccd.mongoHelper.bean.SlowQuery;
import cn.craccd.mongoHelper.bean.SortBuilder;
import cn.craccd.mongoHelper.bean.UpdateBuilder;
import cn.craccd.mongoHelper.bean.UpdateTime;
import cn.craccd.mongoHelper.config.Constant;
import cn.craccd.mongoHelper.reflection.ReflectionUtil;
import cn.craccd.mongoHelper.reflection.SerializableFunction;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;

/**
 * mongodb操作器
 *
 */
@Service("mongoHelper")
public class MongoHelper {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	protected MongoConverter mongoConverter;

	protected QueryMapper queryMapper;
	protected UpdateMapper updateMapper;

	@Autowired
	protected MongoTemplate mongoTemplate;

	public MongoTemplate getMongoTemplate() {
		return mongoTemplate;
	}

	public void setMongoTemplate(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

	@Value("${spring.data.mongodb.print:false}")
	protected Boolean print;

	@Value("${spring.data.mongodb.slowQuery:false}")
	protected Boolean slowQuery;

	@Value("${spring.data.mongodb.slowTime:1000}")
	protected Long slowTime;

	@PostConstruct
	public void init() {
		queryMapper = new QueryMapper(mongoConverter);
		updateMapper = new UpdateMapper(mongoConverter);
	}

	private void insertSlowQuery(String log, Long queryTime) {
		if (slowQuery) {
			SlowQuery slowQuery = new SlowQuery();
			slowQuery.setQuery(log);
			slowQuery.setTime(DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
			slowQuery.setQueryTime(queryTime);
			slowQuery.setSystem(SystemTool.getSystem());
			StackTraceElement stack[] = Thread.currentThread().getStackTrace();

			// 保存堆栈
			String stackStr = "";
			for (int i = 0; i < stack.length; i++) {
				stackStr += stack[i].getClassName() + "." + stack[i].getMethodName() + ":" + stack[i].getLineNumber()
						+ "\n";
			}
			slowQuery.setStack(stackStr);

			mongoTemplate.insert(slowQuery);
		}
	}

	/**
	 * 打印查询语句
	 * 
	 * @param query
	 */
	private void logQuery(Class<?> clazz, Query query, Long startTime) {

		MongoPersistentEntity<?> entity = mongoConverter.getMappingContext().getPersistentEntity(clazz);
		Document mappedQuery = queryMapper.getMappedObject(query.getQueryObject(), entity);
		Document mappedField = queryMapper.getMappedObject(query.getFieldsObject(), entity);
		Document mappedSort = queryMapper.getMappedObject(query.getSortObject(), entity);

		String log = "\ndb." + StrUtil.lowerFirst(clazz.getSimpleName()) + ".find(";

		log += FormatUtils.bson(mappedQuery.toJson()) + ")";

		if (!query.getFieldsObject().isEmpty()) {
			log += ".projection(";
			log += FormatUtils.bson(mappedField.toJson()) + ")";
		}

		if (query.isSorted()) {
			log += ".sort(";
			log += FormatUtils.bson(mappedSort.toJson()) + ")";
		}

		if (query.getLimit() != 0l) {
			log += ".limit(" + query.getLimit() + ")";
		}

		if (query.getSkip() != 0l) {
			log += ".skip(" + query.getSkip() + ")";
		}
		log += ";";

		// 记录慢查询
		Long queryTime = System.currentTimeMillis() - startTime;
		if (queryTime > slowTime) {
			insertSlowQuery(log, queryTime);
		}

		// 打印语句
		if (print) {
			logger.info(log + "\n执行时间:" + queryTime + "ms");
		}

	}

	/**
	 * 打印查询语句
	 * 
	 * @param query
	 */
	private void logCount(Class<?> clazz, Query query, Long startTime) {

		MongoPersistentEntity<?> entity = mongoConverter.getMappingContext().getPersistentEntity(clazz);
		Document mappedQuery = queryMapper.getMappedObject(query.getQueryObject(), entity);

		String log = "\ndb." + StrUtil.lowerFirst(clazz.getSimpleName()) + ".find(";
		log += FormatUtils.bson(mappedQuery.toJson()) + ")";
		log += ".count();";

		// 记录慢查询
		Long queryTime = System.currentTimeMillis() - startTime;
		if (queryTime > slowTime) {
			insertSlowQuery(log, queryTime);
		}

		// 打印语句
		if (print) {
			logger.info(log + "\n执行时间:" + queryTime + "ms");
		}

	}

	/**
	 * 打印查询语句
	 * 
	 * @param query
	 */
	private void logDelete(Class<?> clazz, Query query, Long startTime) {

		MongoPersistentEntity<?> entity = mongoConverter.getMappingContext().getPersistentEntity(clazz);
		Document mappedQuery = queryMapper.getMappedObject(query.getQueryObject(), entity);

		String log = "\ndb." + StrUtil.lowerFirst(clazz.getSimpleName()) + ".remove(";
		log += FormatUtils.bson(mappedQuery.toJson()) + ")";
		log += ";";

		// 记录慢查询
		Long queryTime = System.currentTimeMillis() - startTime;
		if (queryTime > slowTime) {
			insertSlowQuery(log, queryTime);
		}

		// 打印语句
		if (print) {
			logger.info(log + "\n执行时间:" + queryTime + "ms");
		}

	}

	/**
	 * 打印查询语句
	 * 
	 * @param query
	 */
	private void logUpdate(Class<?> clazz, Query query, Update update, boolean multi, Long startTime) {

		MongoPersistentEntity<?> entity = mongoConverter.getMappingContext().getPersistentEntity(clazz);
		Document mappedQuery = queryMapper.getMappedObject(query.getQueryObject(), entity);
		Document mappedUpdate = updateMapper.getMappedObject(update.getUpdateObject(), entity);

		String log = "\ndb." + StrUtil.lowerFirst(clazz.getSimpleName()) + ".update(";
		log += FormatUtils.bson(mappedQuery.toJson()) + ",";
		log += FormatUtils.bson(mappedUpdate.toJson()) + ",";
		log += FormatUtils.bson("{multi:" + multi + "})");
		log += ";";

		// 记录慢查询
		Long queryTime = System.currentTimeMillis() - startTime;
		if (queryTime > slowTime) {
			insertSlowQuery(log, queryTime);
		}
		// 打印语句
		if (print) {
			logger.info(log + "\n执行时间:" + queryTime + "ms");
		}

	}

	/**
	 * 打印查询语句
	 * 
	 * @param object
	 * 
	 * @param query
	 */
	private void logSave(Object object, Long startTime, Boolean isInsert) {
		Object objectClone = BeanUtil.copyProperties(object, object.getClass());
		if (isInsert) {
			ReflectUtil.setFieldValue(objectClone, Constant.ID, null);
		}

		String log = "\ndb." + StrUtil.lowerFirst(objectClone.getClass().getSimpleName()) + ".save(";
		log += JSONUtil.toJsonPrettyStr(objectClone);
		log += ");";

		// 记录慢查询
		Long queryTime = System.currentTimeMillis() - startTime;
		if (queryTime > slowTime) {
			insertSlowQuery(log, queryTime);
		}

		// 打印语句
		if (print) {
			logger.info(log + "\n执行时间:" + queryTime + "ms");
		}
	}

	/**
	 * 打印查询语句
	 * 
	 * @param object
	 * 
	 * @param query
	 */
	private void logSave(List<?> list, Long startTime) {
		Object object = list.get(0);

		List<Object> cloneList = new ArrayList<>();
		for (Object item : list) {
			Object objectClone = BeanUtil.copyProperties(item, object.getClass());
			ReflectUtil.setFieldValue(objectClone, Constant.ID, null);
			cloneList.add(objectClone);
		}

		String log = "\ndb." + StrUtil.lowerFirst(object.getClass().getSimpleName()) + ".save(";
		log += JSONUtil.toJsonPrettyStr(cloneList);
		log += ");";

		// 记录慢查询
		Long queryTime = System.currentTimeMillis() - startTime;
		if (queryTime > slowTime) {
			insertSlowQuery(log, queryTime);
		}

		// 打印语句
		if (print) {
			logger.info(log + "\n执行时间:" + queryTime + "ms");
		}
	}

	/**
	 * 插入或更新
	 * 
	 * @param object 对象
	 */
	public String insertOrUpdate(Object object) {

		Long time = System.currentTimeMillis();
		String id = (String) ReflectUtil.getFieldValue(object, Constant.ID);
		Object objectOrg = StrUtil.isNotEmpty(id) ? findById(id, object.getClass()) : null;

		if (objectOrg == null) {
			// 插入
			// 设置插入时间
			setCreateTime(object, time);
			// 设置更新时间
			setUpdateTime(object, time);

			// 设置默认值
			setDefaultVaule(object);
			// 去除id值
			ReflectUtil.setFieldValue(object, Constant.ID, null);

			mongoTemplate.save(object);
			id = (String) ReflectUtil.getFieldValue(object, Constant.ID);
			logSave(object, time, true);

		} else {
			// 更新
			Field[] fields = ReflectUtil.getFields(object.getClass());
			// 拷贝属性
			for (Field field : fields) {
				if (!field.getName().equals(Constant.ID) && ReflectUtil.getFieldValue(object, field) != null) {
					ReflectUtil.setFieldValue(objectOrg, field, ReflectUtil.getFieldValue(object, field));
				}
			}

			// 设置更新时间
			setUpdateTime(objectOrg, time);
			mongoTemplate.save(objectOrg);
			logSave(objectOrg, time, false);
		}

		return id;
	}

	/**
	 * 插入
	 * 
	 * @param object 对象
	 */
	public String insert(Object object) {
		ReflectUtil.setFieldValue(object, Constant.ID, null);
		insertOrUpdate(object);
		return (String) ReflectUtil.getFieldValue(object, Constant.ID);
	}

	/**
	 * 批量插入
	 * 
	 * @param <T>
	 * 
	 * @param object 对象
	 */
	public <T> void insertAll(List<T> list) {
		Long time = System.currentTimeMillis();

		for (Object object : list) {

			// 去除id以便插入
			ReflectUtil.setFieldValue(object, Constant.ID, null);
			// 设置插入时间
			setCreateTime(object, time);
			// 设置更新时间
			setUpdateTime(object, time);
			// 设置默认值
			setDefaultVaule(object);
		}

		mongoTemplate.insertAll(list);
		logSave(list, time);

	}

	/**
	 * 设置更新时间
	 * 
	 * @param object 对象
	 */
	private void setUpdateTime(Object object, Long time) {
		Field[] fields = ReflectUtil.getFields(object.getClass());
		for (Field field : fields) {
			// 获取注解
			if (field.isAnnotationPresent(UpdateTime.class) && field.getType().equals(Long.class)) {
				ReflectUtil.setFieldValue(object, field, time);
			}
		}
	}

	/**
	 * 设置创建时间
	 * 
	 * @param object 对象
	 */
	private void setCreateTime(Object object, Long time) {
		Field[] fields = ReflectUtil.getFields(object.getClass());
		for (Field field : fields) {
			// 获取注解
			if (field.isAnnotationPresent(CreateTime.class) && field.getType().equals(Long.class)) {
				ReflectUtil.setFieldValue(object, field, time);
			}
		}
	}

	/**
	 * 根据id更新
	 * 
	 * @param object 对象
	 */
	public void updateById(Object object) {
		if (StrUtil.isEmpty((String) ReflectUtil.getFieldValue(object, Constant.ID))) {
			return;
		}
		if (findById((String) ReflectUtil.getFieldValue(object, Constant.ID), object.getClass()) == null) {
			return;
		}
		insertOrUpdate(object);
	}

	/**
	 * 根据id更新全部字段
	 * 
	 * @param object 对象
	 */
	public void updateAllColumnById(Object object) {

		if (StrUtil.isEmpty((String) ReflectUtil.getFieldValue(object, Constant.ID))) {
			return;
		}
		if (findById((String) ReflectUtil.getFieldValue(object, Constant.ID), object.getClass()) == null) {
			return;
		}
		Long time = System.currentTimeMillis();
		setUpdateTime(object, time);
		mongoTemplate.save(object);
		logSave(object, time, false);
	}

	/**
	 * 更新查到的第一项
	 * 
	 * @param query  查询
	 * @param update 更新
	 * @param clazz  类
	 */
	public void updateFirst(Query query, Update update, Class<?> clazz) {
		Long time = System.currentTimeMillis();
		mongoTemplate.updateFirst(query, update, clazz);
		logUpdate(clazz, query, update, false, time);
	}

	/**
	 * 更新查到的第一项
	 * 
	 * @param query  查询
	 * @param update 更新
	 * @param clazz  类
	 */
	public void updateFirst(Query query, UpdateBuilder updateBuilder, Class<?> clazz) {
		updateFirst(query, updateBuilder.toUpdate(), clazz);
	}

	/**
	 * 更新查到的第一项
	 * 
	 * @param criteria 查询
	 * @param update   更新
	 * @param clazz    类
	 */
	public void updateFirst(Criteria criteria, Update update, Class<?> clazz) {
		updateFirst(new Query(criteria), update, clazz);
	}

	/**
	 * 更新查到的第一项
	 * 
	 * @param criteria 查询
	 * @param update   更新
	 * @param clazz    类
	 */
	public void updateFirst(Criteria criteria, UpdateBuilder updateBuilder, Class<?> clazz) {
		updateFirst(new Query(criteria), updateBuilder.toUpdate(), clazz);
	}

	/**
	 * 更新查到的第一项
	 * 
	 * @param criteria 查询
	 * @param update   更新
	 * @param clazz    类
	 */
	public void updateFirst(CriteriaWrapper criteriaWrapper, Update update, Class<?> clazz) {
		updateFirst(new Query(criteriaWrapper.build()), update, clazz);
	}

	/**
	 * 更新查到的第一项
	 * 
	 * @param criteria 查询
	 * @param update   更新
	 * @param clazz    类
	 */
	public void updateFirst(CriteriaWrapper criteriaWrapper, UpdateBuilder updateBuilder, Class<?> clazz) {
		updateFirst(new Query(criteriaWrapper.build()), updateBuilder, clazz);
	}

	/**
	 * 更新查到的全部项
	 * 
	 * @param query  查询
	 * @param update 更新
	 * @param clazz  类
	 */
	public void updateMulti(Query query, Update update, Class<?> clazz) {
		Long time = System.currentTimeMillis();
		mongoTemplate.updateMulti(query, update, clazz);
		logUpdate(clazz, query, update, true, time);
	}

	/**
	 * 更新查到的全部项
	 * 
	 * @param query  查询
	 * @param update 更新
	 * @param clazz  类
	 */
	public void updateMulti(Query query, UpdateBuilder updateBuilder, Class<?> clazz) {
		updateMulti(query, updateBuilder.toUpdate(), clazz);
	}

	/**
	 * 更新查到的全部项
	 * 
	 * @param criteria 查询
	 * @param update   更新
	 * @param clazz    类
	 */
	public void updateMulti(Criteria criteria, Update update, Class<?> clazz) {
		updateMulti(new Query(criteria), update, clazz);
	}

	/**
	 * 更新查到的全部项
	 * 
	 * @param criteria 查询
	 * @param update   更新
	 * @param clazz    类
	 */
	public void updateMulti(Criteria criteria, UpdateBuilder updateBuilder, Class<?> clazz) {
		updateMulti(new Query(criteria), updateBuilder.toUpdate(), clazz);
	}

	/**
	 * 更新查到的全部项
	 * 
	 * @param criteria 查询
	 * @param update   更新
	 * @param clazz    类
	 */
	public void updateMulti(CriteriaWrapper criteriaWrapper, Update update, Class<?> clazz) {
		updateMulti(new Query(criteriaWrapper.build()), update, clazz);
	}

	/**
	 * 更新查到的全部项
	 * 
	 * @param criteria 查询
	 * @param update   更新
	 * @param clazz    类
	 */
	public void updateMulti(CriteriaWrapper criteriaWrapper, UpdateBuilder updateBuilder, Class<?> clazz) {
		updateMulti(new Query(criteriaWrapper.build()), updateBuilder.toUpdate(), clazz);
	}

	/**
	 * 根据id删除
	 * 
	 * @param id    对象
	 * @param clazz 类
	 */
	public void deleteById(String id, Class<?> clazz) {

		if (StrUtil.isEmpty(id)) {
			return;
		}
		deleteByQuery(Criteria.where(Constant.ID).is(id), clazz);
	}

	/**
	 * 根据id删除
	 * 
	 * @param id    对象
	 * @param clazz 类
	 */
	public void deleteByIds(List<String> ids, Class<?> clazz) {

		if (ids == null || ids.size() == 0) {
			return;
		}

		deleteByQuery(Criteria.where(Constant.ID).in(ids), clazz);
	}

	/**
	 * 根据条件删除
	 * 
	 * @param query 查询
	 * @param clazz 类
	 */
	public void deleteByQuery(Query query, Class<?> clazz) {
		Long time = System.currentTimeMillis();
		mongoTemplate.remove(query, clazz);
		logDelete(clazz, query, time);
	}

	/**
	 * 根据条件删除
	 * 
	 * @param criteria 查询
	 * @param clazz    类
	 */
	public void deleteByQuery(Criteria criteria, Class<?> clazz) {
		deleteByQuery(new Query(criteria), clazz);
	}

	/**
	 * 根据条件删除
	 * 
	 * @param criteria 查询
	 * @param clazz    类
	 */
	public void deleteByQuery(CriteriaWrapper criteriaWrapper, Class<?> clazz) {
		deleteByQuery(new Query(criteriaWrapper.build()), clazz);
	}

	/**
	 * 设置默认值
	 * 
	 * @param object 对象
	 */
	private void setDefaultVaule(Object object) {
		Field[] fields = ReflectUtil.getFields(object.getClass());
		for (Field field : fields) {
			// 获取注解
			if (field.isAnnotationPresent(InitValue.class)) {
				InitValue defaultValue = field.getAnnotation(InitValue.class);

				String value = defaultValue.value();

				if (ReflectUtil.getFieldValue(object, field) == null) {
					// 获取字段类型
					Class<?> type = field.getType();
					if (type.equals(String.class)) {
						ReflectUtil.setFieldValue(object, field, value);
					}
					if (type.equals(Short.class)) {
						ReflectUtil.setFieldValue(object, field, Short.parseShort(value));
					}
					if (type.equals(Integer.class)) {
						ReflectUtil.setFieldValue(object, field, Integer.parseInt(value));
					}
					if (type.equals(Long.class)) {
						ReflectUtil.setFieldValue(object, field, Long.parseLong(value));
					}
					if (type.equals(Float.class)) {
						ReflectUtil.setFieldValue(object, field, Float.parseFloat(value));
					}
					if (type.equals(Double.class)) {
						ReflectUtil.setFieldValue(object, field, Double.parseDouble(value));
					}
					if (type.equals(Boolean.class)) {
						ReflectUtil.setFieldValue(object, field, Boolean.parseBoolean(value));
					}
				}
			}
		}
	}

	/**
	 * 累加某一个字段的数量,原子操作
	 * 
	 * @param object
	 */
	public void addCountById(String id, String property, Number count, Class<?> clazz) {
		Update update = new Update().inc(property, count);
		updateFirst(new Query(Criteria.where(Constant.ID).is(id)), update, clazz);
	}

	/**
	 * 累加某一个字段的数量,原子操作
	 * 
	 * @param object
	 */
	public <T, R> void addCountById(String id, SerializableFunction<T, R> property, Number count, Class<?> clazz) {
		addCountById(id, ReflectionUtil.getFieldName(property), count, clazz);
	}

	/**
	 * 按查询条件获取Page
	 * 
	 * @param query 查询
	 * @param page  分页
	 * @param clazz 类
	 * @return Page 分页
	 */
	public <T> Page<T> findPage(Query query, Page<?> page, Class<T> clazz) {

		Page<T> pageResp = new Page<T>();
		pageResp.setCurr(page.getCurr());
		pageResp.setLimit(page.getLimit());

		// 查询出一共的条数
		Long count = findCountByQuery(query, clazz);
		pageResp.setCount(count);

		// 查询List
		query.skip((page.getCurr() - 1) * page.getLimit());// 从那条记录开始
		query.limit(page.getLimit());// 取多少条记录

		List<T> list = findListByQuery(query, clazz);
		pageResp.setList(list);

		return pageResp;

	}

	/**
	 * 按查询条件获取Page
	 * 
	 * @param criteria 查询
	 * @param page     分页
	 * @param clazz    类
	 * @return Page 分页
	 */
	public <T> Page<T> findPage(Criteria criteria, Page<?> page, Class<T> clazz) {
		return findPage(new Query(criteria), page, clazz);
	}

	/**
	 * 按查询条件获取Page
	 * 
	 * @param criteria 查询
	 * @param page     分页
	 * @param clazz    类
	 * @return Page 分页
	 */
	public <T> Page<T> findPage(CriteriaWrapper criteriaWrapper, Page<?> page, Class<T> clazz) {
		return findPage(new Query(criteriaWrapper.build()), page, clazz);
	}

	/**
	 * 按查询条件获取Page
	 * 
	 * @param criteria 查询
	 * @param sort     排序
	 * @param clazz    类
	 * @return Page 分页
	 */
	public <T> Page<T> findPage(Criteria criteria, Sort sort, Page<?> page, Class<T> clazz) {
		return findPage(new Query(criteria).with(sort), page, clazz);
	}

	/**
	 * 按查询条件获取Page
	 * 
	 * @param criteria 查询
	 * @param sort     排序
	 * @param clazz    类
	 * @return Page 分页
	 */
	public <T> Page<T> findPage(Criteria criteria, SortBuilder sortBuilder, Page<?> page, Class<T> clazz) {
		return findPage(new Query(criteria).with(sortBuilder.toSort()), page, clazz);
	}

	/**
	 * 按查询条件获取Page
	 * 
	 * @param criteria 查询
	 * @param sort     排序
	 * @param clazz    类
	 * @return Page 分页
	 */
	public <T> Page<T> findPage(CriteriaWrapper criteriaWrapper, SortBuilder sortBuilder, Page<?> page,
			Class<T> clazz) {
		return findPage(new Query(criteriaWrapper.build()).with(sortBuilder.toSort()), page, clazz);
	}

	/**
	 * 按查询条件获取Page
	 * 
	 * @param criteria 查询
	 * @param sort     排序
	 * @param clazz    类
	 * @return Page 分页
	 */
	public <T> Page<T> findPage(CriteriaWrapper criteriaWrapper, Sort sort, Page<?> page, Class<T> clazz) {
		return findPage(new Query(criteriaWrapper.build()).with(sort), page, clazz);
	}

	/**
	 * 按查询条件获取Page
	 * 
	 * @param criteria 查询
	 * @param sort     排序
	 * @param clazz    类
	 * @return Page 分页
	 */
	public <T> Page<T> findPage(Sort sort, Page<?> page, Class<T> clazz) {
		return findPage(new Query().with(sort), page, clazz);
	}

	/**
	 * 按查询条件获取Page
	 * 
	 * @param criteria 查询
	 * @param sort     排序
	 * @param clazz    类
	 * @return Page 分页
	 */
	public <T> Page<T> findPage(SortBuilder sortBuilder, Page<?> page, Class<T> clazz) {
		return findPage(new Query().with(sortBuilder.toSort()), page, clazz);
	}

	/**
	 * 获取Page
	 * 
	 * @param page  分页
	 * @param clazz 类
	 * @return Page 分页
	 */
	public <T> Page<T> findPage(Page<?> page, Class<T> clazz) {
		return findPage(new Query(), page, clazz);
	}

	/**
	 * 根据id查找
	 * 
	 * @param id    id
	 * @param clazz 类
	 * @return T 对象
	 */
	public <T> T findById(String id, Class<T> clazz) {

		if (StrUtil.isEmpty(id)) {
			return null;
		}
		Long systemTime = System.currentTimeMillis();

		T t = (T) mongoTemplate.findById(id, clazz);
		logQuery(clazz, new Query(Criteria.where(Constant.ID).is(id)), systemTime);
		return t;
	}

	/**
	 * 根据条件查找单个
	 * 
	 * @param query 查询
	 * @param clazz 类
	 * @return T 对象
	 */
	public <T> T findOneByQuery(Query query, Class<T> clazz) {
		query.limit(1);

		Long systemTime = System.currentTimeMillis();
		T t = (T) mongoTemplate.findOne(query, clazz);
		logQuery(clazz, query, systemTime);

		return t;
	}

	/**
	 * 根据条件查找单个
	 * 
	 * @param <T>      类型
	 * @param criteria
	 * @param clazz    类
	 * @return T 对象
	 */
	public <T> T findOneByQuery(Criteria criteria, Class<T> clazz) {
		return (T) findOneByQuery(new Query(criteria), clazz);
	}

	/**
	 * 根据条件查找单个
	 * 
	 * @param <T>      类型
	 * @param criteria
	 * @param clazz    类
	 * @return T 对象
	 */
	public <T> T findOneByQuery(CriteriaWrapper criteriaWrapper, Class<T> clazz) {
		return (T) findOneByQuery(new Query(criteriaWrapper.build()), clazz);
	}

	/**
	 * 根据条件查找单个
	 * 
	 * @param query 查询
	 * @param clazz 类
	 * @return T 对象
	 */
	public <T> T findOneByQuery(Criteria criteria, Sort sort, Class<T> clazz) {
		return (T) findOneByQuery(new Query(criteria).with(sort), clazz);
	}

	/**
	 * 根据条件查找单个
	 * 
	 * @param query 查询
	 * @param clazz 类
	 * @return T 对象
	 */
	public <T> T findOneByQuery(Criteria criteria, SortBuilder sortBuilder, Class<T> clazz) {
		return (T) findOneByQuery(new Query(criteria).with(sortBuilder.toSort()), clazz);
	}

	/**
	 * 根据条件查找单个
	 * 
	 * @param query 查询
	 * @param clazz 类
	 * @return T 对象
	 */
	public <T> T findOneByQuery(CriteriaWrapper criteriaWrapper, Sort sort, Class<T> clazz) {
		return (T) findOneByQuery(new Query(criteriaWrapper.build()).with(sort), clazz);
	}

	/**
	 * 根据条件查找单个
	 * 
	 * @param query 查询
	 * @param clazz 类
	 * @return T 对象
	 */
	public <T> T findOneByQuery(CriteriaWrapper criteriaWrapper, SortBuilder sortBuilder, Class<T> clazz) {
		return (T) findOneByQuery(new Query(criteriaWrapper.build()).with(sortBuilder.toSort()), clazz);
	}

	/**
	 * 根据条件查找单个
	 * 
	 * @param query 查询
	 * @param clazz 类
	 * @return T 对象
	 */
	public <T> T findOneByQuery(Sort sort, Class<T> clazz) {
		return (T) findOneByQuery(new Query().with(sort), clazz);
	}

	/**
	 * 根据条件查找单个
	 * 
	 * @param query 查询
	 * @param clazz 类
	 * @return T 对象
	 */
	public <T> T findOneByQuery(SortBuilder sortBuilder, Class<T> clazz) {
		return (T) findOneByQuery(new Query().with(sortBuilder.toSort()), clazz);
	}

	/**
	 * 根据条件查找List
	 * 
	 * @param <T>   类型
	 * @param query 查询
	 * @param clazz 类
	 * @return List 列表
	 */
	public <T> List<T> findListByQuery(Query query, Class<T> clazz) {
		if (!query.isSorted()) {
			query.with(Sort.by(Sort.Direction.DESC, Constant.ID));
		}

		Long systemTime = System.currentTimeMillis();
		List<T> list = mongoTemplate.find(query, clazz);
		logQuery(clazz, query, systemTime);
		return list;
	}

	/**
	 * 根据条件查找List
	 * 
	 * @param <T>      类型
	 * @param criteria 查询
	 * @param clazz    类
	 * @return List 列表
	 */
	public <T> List<T> findListByQuery(Criteria criteria, Class<T> clazz) {
		return (List<T>) findListByQuery(new Query(criteria), clazz);
	}

	/**
	 * 根据条件查找List
	 * 
	 * @param <T>      类型
	 * @param criteria 查询
	 * @param clazz    类
	 * @return List 列表
	 */
	public <T> List<T> findListByQuery(CriteriaWrapper criteriaWrapper, Class<T> clazz) {
		return (List<T>) findListByQuery(new Query(criteriaWrapper.build()), clazz);
	}

	/**
	 * 根据条件查找List
	 * 
	 * @param <T>      类型
	 * @param criteria 查询
	 * @param sort     排序
	 * @param clazz    类
	 * @return List 列表
	 */
	public <T> List<T> findListByQuery(Criteria criteria, SortBuilder sortBuilder, Class<T> clazz) {
		return (List<T>) findListByQuery(new Query(criteria).with(sortBuilder.toSort()), clazz);
	}

	/**
	 * 根据条件查找List
	 * 
	 * @param <T>      类型
	 * @param criteria 查询
	 * @param sort     排序
	 * @param clazz    类
	 * @return List 列表
	 */
	public <T> List<T> findListByQuery(Criteria criteria, Sort sort, Class<T> clazz) {
		return (List<T>) findListByQuery(new Query(criteria).with(sort), clazz);
	}

	/**
	 * 根据条件查找List
	 * 
	 * @param <T>      类型
	 * @param criteria 查询
	 * @param sort     排序
	 * @param clazz    类
	 * @return List 列表
	 */
	public <T> List<T> findListByQuery(CriteriaWrapper criteriaWrapper, SortBuilder sortBuilder, Class<T> clazz) {
		return (List<T>) findListByQuery(new Query(criteriaWrapper.build()).with(sortBuilder.toSort()), clazz);
	}

	/**
	 * 根据条件查找List
	 * 
	 * @param <T>      类型
	 * @param criteria 查询
	 * @param sort     排序
	 * @param clazz    类
	 * @return List 列表
	 */
	public <T> List<T> findListByQuery(CriteriaWrapper criteriaWrapper, Sort sort, Class<T> clazz) {
		return (List<T>) findListByQuery(new Query(criteriaWrapper.build()).with(sort), clazz);
	}

	/**
	 * 根据条件查找某个属性
	 * 
	 * @param <T>           类型
	 * @param query         查询
	 * @param documentClass 类
	 * @param property      属性
	 * @param propertyClass 属性类
	 * @return List 列表
	 */
	public <T> List<T> findPropertiesByQuery(Query query, Class<?> documentClass, String property,
			Class<T> propertyClass) {
		query.fields().include(property);
		List<?> list = findListByQuery(query, documentClass);
		List<T> propertyList = extractProperty(list, property, propertyClass);

		return propertyList;
	}

	/**
	 * 根据条件查找某个属性
	 * 
	 * @param <T>           类型
	 * @param query         查询
	 * @param documentClass 类
	 * @param property      属性
	 * @param propertyClass 属性类
	 * @return List 列表
	 */
	public <T, R> List<T> findPropertiesByQuery(Query query, Class<?> documentClass,
			SerializableFunction<T, R> property, Class<T> propertyClass) {
		return findPropertiesByQuery(query, documentClass, ReflectionUtil.getFieldName(property), propertyClass);
	}

	/**
	 * 根据条件查找某个属性
	 * 
	 * @param <T>           类型
	 * @param criteria      查询
	 * @param documentClass 类
	 * @param property      属性
	 * @return List 列表
	 */
	public List<String> findPropertiesByQuery(Query query, Class<?> documentClass, String property) {
		return findPropertiesByQuery(query, documentClass, property, String.class);
	}

	/**
	 * 根据条件查找某个属性
	 * 
	 * @param <T>           类型
	 * @param criteria      查询
	 * @param documentClass 类
	 * @param property      属性
	 * @return List 列表
	 */
	public <T, R> List<String> findPropertiesByQuery(Query query, Class<?> documentClass,
			SerializableFunction<T, R> property) {
		return findPropertiesByQuery(query, documentClass, ReflectionUtil.getFieldName(property), String.class);
	}

	/**
	 * 根据条件查找某个属性
	 * 
	 * @param <T>           类型
	 * @param criteria      查询
	 * @param documentClass 类
	 * @param property      属性
	 * @param propertyClass 属性类
	 * @return List 列表
	 */
	public <T> List<T> findPropertiesByQuery(Criteria criteria, Class<?> documentClass, String property,
			Class<T> propertyClass) {
		return (List<T>) findPropertiesByQuery(new Query(criteria), documentClass, property, propertyClass);
	}

	/**
	 * 根据条件查找某个属性
	 * 
	 * @param <T>           类型
	 * @param criteria      查询
	 * @param documentClass 类
	 * @param property      属性
	 * @param propertyClass 属性类
	 * @return List 列表
	 */
	public <T, R> List<T> findPropertiesByQuery(Criteria criteria, Class<?> documentClass,
			SerializableFunction<T, R> property, Class<T> propertyClass) {
		return (List<T>) findPropertiesByQuery(new Query(criteria), documentClass,
				ReflectionUtil.getFieldName(property), propertyClass);
	}

	/**
	 * 根据条件查找某个属性
	 * 
	 * @param <T>           类型
	 * @param criteria      查询
	 * @param documentClass 类
	 * @param property      属性
	 * @param propertyClass 属性类
	 * @return List 列表
	 */
	public <T, R> List<T> findPropertiesByQuery(CriteriaWrapper criteriaWrapper, Class<?> documentClass,
			SerializableFunction<T, R> property, Class<T> propertyClass) {
		return (List<T>) findPropertiesByQuery(new Query(criteriaWrapper.build()), documentClass,
				ReflectionUtil.getFieldName(property), propertyClass);
	}

	/**
	 * 根据条件查找某个属性
	 * 
	 * @param <T>           类型
	 * @param criteria      查询
	 * @param documentClass 类
	 * @param property      属性
	 * @param propertyClass 属性类
	 * @return List 列表
	 */
	public <T> List<T> findPropertiesByQuery(CriteriaWrapper criteriaWrapper, Class<?> documentClass, String property,
			Class<T> propertyClass) {
		return (List<T>) findPropertiesByQuery(new Query(criteriaWrapper.build()), documentClass, property,
				propertyClass);
	}

	/**
	 * 根据条件查找某个属性
	 * 
	 * @param <T>           类型
	 * @param criteria      查询
	 * @param documentClass 类
	 * @param property      属性
	 * @return List 列表
	 */
	public List<String> findPropertiesByQuery(Criteria criteria, Class<?> documentClass, String property) {
		return findPropertiesByQuery(new Query(criteria), documentClass, property, String.class);
	}

	/**
	 * 根据条件查找某个属性
	 * 
	 * @param <T>           类型
	 * @param criteria      查询
	 * @param documentClass 类
	 * @param property      属性
	 * @return List 列表
	 */
	public <T, R> List<String> findPropertiesByQuery(Criteria criteria, Class<?> documentClass,
			SerializableFunction<T, R> property) {
		return findPropertiesByQuery(new Query(criteria), documentClass, ReflectionUtil.getFieldName(property),
				String.class);
	}

	/**
	 * 根据条件查找某个属性
	 * 
	 * @param <T>           类型
	 * @param criteria      查询
	 * @param documentClass 类
	 * @param property      属性
	 * @return List 列表
	 */
	public List<String> findPropertiesByQuery(CriteriaWrapper criteriaWrapper, Class<?> documentClass,
			String property) {
		return findPropertiesByQuery(new Query(criteriaWrapper.build()), documentClass, property, String.class);
	}

	/**
	 * 根据条件查找某个属性
	 * 
	 * @param <T>           类型
	 * @param criteria      查询
	 * @param documentClass 类
	 * @param property      属性
	 * @return List 列表
	 */
	public <T, R> List<String> findPropertiesByQuery(CriteriaWrapper criteriaWrapper, Class<?> documentClass,
			SerializableFunction<T, R> property) {
		return findPropertiesByQuery(new Query(criteriaWrapper.build()), documentClass,
				ReflectionUtil.getFieldName(property), String.class);
	}

	/**
	 * 根据条件查找某个属性
	 * 
	 * @param <T>           类型
	 * @param criteria      查询
	 * @param documentClass 类
	 * @param property      属性
	 * @return List 列表
	 */
	public List<String> findPropertiesByIds(Collection<String> ids, Class<?> documentClass, String property) {
		return findPropertiesByQuery(Criteria.where(Constant.ID).in(ids), documentClass, property, String.class);
	}

	/**
	 * 根据条件查找某个属性
	 * 
	 * @param <T>           类型
	 * @param criteria      查询
	 * @param documentClass 类
	 * @param property      属性
	 * @return List 列表
	 */
	public <T, R> List<String> findPropertiesByIds(Collection<String> ids, Class<?> documentClass,
			SerializableFunction<T, R> property) {
		return findPropertiesByIds(ids, documentClass, ReflectionUtil.getFieldName(property));
	}

	/**
	 * 根据条件查找id
	 * 
	 * @param query 查询
	 * @param clazz 类
	 * @return List 列表
	 */
	public List<String> findIdsByQuery(Query query, Class<?> clazz) {
		List<String> ids = new ArrayList<String>();
		query.fields().include(Constant.ID);
		List<?> list = findListByQuery(query, clazz);
		for (Object object : list) {
			ids.add((String) ReflectUtil.getFieldValue(object, Constant.ID));
		}

		return ids;
	}

	/**
	 * 根据条件查找id
	 * 
	 * @param criteria 查询
	 * @param clazz    类
	 * @return List 列表
	 */
	public List<String> findIdsByQuery(Criteria criteria, Class<?> clazz) {
		return findIdsByQuery(new Query(criteria), clazz);
	}

	/**
	 * 根据条件查找id
	 * 
	 * @param criteria 查询
	 * @param clazz    类
	 * @return List 列表
	 */
	public List<String> findIdsByQuery(CriteriaWrapper criteriaWrapper, Class<?> clazz) {
		return findIdsByQuery(new Query(criteriaWrapper.build()), clazz);
	}

	/**
	 * 根据条件查找id
	 * 
	 * @param criteria 查询
	 * @param clazz    类
	 * @return List 列表
	 */
	public List<String> findIdsByQuery(Criteria criteria, Sort sort, Class<?> clazz) {
		return findIdsByQuery(new Query(criteria).with(sort), clazz);
	}

	/**
	 * 根据条件查找id
	 * 
	 * @param criteria 查询
	 * @param clazz    类
	 * @return List 列表
	 */
	public List<String> findIdsByQuery(Criteria criteria, SortBuilder sortBuilder, Class<?> clazz) {
		return findIdsByQuery(new Query(criteria).with(sortBuilder.toSort()), clazz);
	}

	/**
	 * 根据条件查找id
	 * 
	 * @param criteria 查询
	 * @param clazz    类
	 * @return List 列表
	 */
	public List<String> findIdsByQuery(CriteriaWrapper criteriaWrapper, Sort sort, Class<?> clazz) {
		return findIdsByQuery(new Query(criteriaWrapper.build()).with(sort), clazz);
	}

	/**
	 * 根据条件查找id
	 * 
	 * @param criteria 查询
	 * @param clazz    类
	 * @return List 列表
	 */
	public List<String> findIdsByQuery(CriteriaWrapper criteriaWrapper, SortBuilder sortBuilder, Class<?> clazz) {
		return findIdsByQuery(new Query(criteriaWrapper.build()).with(sortBuilder.toSort()), clazz);
	}

	/**
	 * 根据id集合查找
	 * 
	 * @param List  ids id集合
	 * @param clazz 类
	 * @return List 列表
	 */
	public <T> List<T> findListByIds(Collection<String> ids, Class<T> clazz) {
		return findListByQuery(new Query(Criteria.where(Constant.ID).in(ids)), clazz);
	}

	/**
	 * 根据id集合查找
	 * 
	 * @param List  ids id集合
	 * @param clazz 类
	 * @return List 列表
	 */
	public <T> List<T> findListByIds(Collection<String> ids, SortBuilder sortBuilder, Class<T> clazz) {
		return findListByQuery(new Query(Criteria.where(Constant.ID).in(ids)).with(sortBuilder.toSort()), clazz);
	}

	/**
	 * 根据id集合查找
	 * 
	 * @param List  ids id集合
	 * @param clazz 类
	 * @return List 列表
	 */
	public <T> List<T> findListByIds(Collection<String> ids, Sort sort, Class<T> clazz) {
		return findListByQuery(new Query(Criteria.where(Constant.ID).in(ids)).with(sort), clazz);
	}

	/**
	 * 根据id集合查找
	 * 
	 * @param Array ids id集合
	 * @param clazz 类
	 * @return List 列表
	 */
	public <T> List<T> findListByIds(String[] ids, SortBuilder sortBuilder, Class<T> clazz) {
		return findListByQuery(new Query(Criteria.where(Constant.ID).in(strToObj(ids))).with(sortBuilder.toSort()), clazz);
	}

	/**
	 * 根据id集合查找
	 * 
	 * @param Array ids id集合
	 * @param clazz 类
	 * @return List 列表
	 */
	public <T> List<T> findListByIds(String[] ids, Sort sort, Class<T> clazz) {
		return findListByQuery(new Query(Criteria.where(Constant.ID).in(strToObj(ids))).with(sort), clazz);
	}

	

	/**
	 * 根据id集合查找
	 * 
	 * @param Array ids id集合
	 * @param clazz 类
	 * @return List 列表
	 */
	public <T> List<T> findListByIds(String[] ids, Class<T> clazz) {
		return findListByQuery(new Query(Criteria.where(Constant.ID).in(strToObj(ids))), clazz);
	}

	/**
	 * 查询全部
	 * 
	 * @param <T>   类型
	 * @param clazz 类
	 * @return List 列表
	 */
	public <T> List<T> findAll(Class<T> clazz) {
		return findListByQuery(new Query(), clazz);
	}

	/**
	 * 查询全部
	 * 
	 * @param <T>   类型
	 * @param clazz 类
	 * @return List 列表
	 */
	public <T> List<T> findAll(Sort sort, Class<T> clazz) {
		return findListByQuery(new Criteria(), sort, clazz);
	}

	/**
	 * 查询全部
	 * 
	 * @param <T>   类型
	 * @param clazz 类
	 * @return List 列表
	 */
	public <T> List<T> findAll(SortBuilder sortBuilder, Class<T> clazz) {
		return findListByQuery(new Criteria(), sortBuilder.toSort(), clazz);
	}

	/**
	 * 查找全部的id
	 * 
	 * @param clazz 类
	 * @return List 列表
	 */
	public List<String> findAllIds(Class<?> clazz) {
		return findIdsByQuery(new Query(), clazz);
	}

	/**
	 * 查找数量
	 * 
	 * @param query 查询
	 * @param clazz 类
	 * @return Long 数量
	 */
	public Long findCountByQuery(Query query, Class<?> clazz) {

		Long systemTime = System.currentTimeMillis();
		Long count = null;

		if (query.getQueryObject().isEmpty()) {
			count = mongoTemplate.getCollection(mongoTemplate.getCollectionName(clazz)).estimatedDocumentCount();
		} else {
			count = mongoTemplate.count(query, clazz);
		}

		logCount(clazz, query, systemTime);
		return count;
	}

	/**
	 * 查找数量
	 * 
	 * @param criteria 查询
	 * @param clazz    类
	 * @return Long 数量
	 */
	public Long findCountByQuery(Criteria criteria, Class<?> clazz) {
		return findCountByQuery(new Query(criteria), clazz);
	}

	/**
	 * 查找数量
	 * 
	 * @param criteria 查询
	 * @param clazz    类
	 * @return Long 数量
	 */
	public Long findCountByQuery(CriteriaWrapper criteriaWrapper, Class<?> clazz) {
		return findCountByQuery(new Query(criteriaWrapper.build()), clazz);
	}

	/**
	 * 查找全部数量
	 * 
	 * @param clazz 类
	 * @return Long 数量
	 */
	public Long findAllCount(Class<?> clazz) {
		return findCountByQuery(new Query(), clazz);
	}

	/**
	 * 获取list中对象某个属性,组成新的list
	 * 
	 * @param list     列表
	 * @param clazz    类
	 * @param property 属性
	 * @return List<T> 列表
	 */
	@SuppressWarnings("unchecked")
	private <T> List<T> extractProperty(List<?> list, String property, Class<T> clazz) {
		Set<T> rs = new HashSet<T>();
		for (Object object : list) {
			Object value = ReflectUtil.getFieldValue(object, property);
			if (value != null && value.getClass().equals(clazz)) {
				rs.add((T) value);
			}
		}

		return new ArrayList<T>(rs);
	}
	
	/**
	 * String数组转Object数组
	 * @param ids
	 * @return
	 */
	private Object[] strToObj(String[] ids) {
		Object[] objects = new Object[ids.length];
		for (int i = 0; i < ids.length; i++) {
			objects[i] = ids[i];
		}
		return objects;
	}

}
